//
//  NavController.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 05/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit

enum ViewController{
    case repositories
    case pullRequests(PullRequestDataSource)
    
    var instantiated:UIViewController{
        switch self {
        case .repositories:
            guard let repVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RepositoriesDetailsViewController") as? RepositoriesViewController
                else{
                    fatalError("Error dequeing RepositoriesViewController")
            }
            return repVc
        case .pullRequests(let prDataSource):
                guard let prVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PullRequestsViewController") as? PullRequestsViewController
                    else{
                        fatalError("Error dequeing PullRequestsViewController")
                }
                prVc.dataSource = prDataSource
                return prVc

        }
    }
}
final class NavController: UINavigationController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func push(viewController:ViewController){
        pushViewController(viewController.instantiated, animated: true)
    }
}
