//
//  RepositoriesViewAdapter.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 07/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit
import PKHUD

final class RepositoriesViewAdapter:NSObject{
    internal var tableView:UITableView
    private weak var dataSource:RepositoriesDataSource?
    
    
    //IDENTIFIER MUST BE THE SAME NAME AS THE NIB FILE
    init(tableView:UITableView, dataSource:RepositoriesDataSource? = nil, identifier:String){
        self.tableView = tableView

        self.dataSource = dataSource
        super.init()
        self.dataSource?.activityDelegate = self
        self.tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
}
extension RepositoriesViewAdapter:TableViewAdapter{
    func updateData() {
        tableView.reloadData()
    }
}
extension RepositoriesViewAdapter:UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        return dataSource?.cellConfig(indexPath.row, tableView) ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return dataSource?.numberOfItems ?? 0
    }
}
extension RepositoriesViewAdapter:UITableViewDelegate{
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let repository = dataSource?.dataForItemNumber(indexPath.row){
            dataSource?.retrievePullRequestOfRepository(repository)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (dataSource?.numberOfItems ?? 0)-5 {
            dataSource?.retrieveNextRepositories()
        }
    }
}
extension RepositoriesViewAdapter:ActivitiyIndicator{
    func didStartActivity() {
        DispatchQueue.main.async {
            
            HUD.show(.progress, onView: self.tableView)
        }
    }
    
    func didFinishActivity() {
        HUD.hide()
    }
    
    
}
