//
//  RepositoriesViewController.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 05/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit

final class RepositoriesViewController: UIViewController {

    var dataSource:RepositoriesDataSource?
    @IBOutlet weak var tableView: UITableView!
    let CellIdentifier:String = "RepositoryTableViewCell"
    var tableViewAdapter:RepositoriesViewAdapter?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = RepositoriesDataSource(cellConfig: {[weak self] (row, tableView) -> (UITableViewCell?) in
            if let identifier = self?.CellIdentifier, let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: IndexPath(row: row, section: 0)) as? RepositoryTableViewCell{
                cell.cellData = self?.dataSource?.dataForItemNumber(row)
                return cell
            }
            return nil
        })
        dataSource?.dataManagerDelegate = self
        tableViewAdapter = RepositoriesViewAdapter(tableView: tableView, dataSource: dataSource, identifier:CellIdentifier)
        dataSource?.retrieveNextRepositories()
    }


}
extension RepositoriesViewController:DataManager{
    func didReceiveData(data: Any?) {
        if let _ = data as? RepositoriesRoot{
            tableViewAdapter?.updateData()
        }
        else if let pullRequests = data as? [PullRequest]{
            let prDataSource = PullRequestDataSource(pullRequests: pullRequests, cellConfig: { (row, tableView) -> (UITableViewCell?) in
                if let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestTableViewCell", for: IndexPath(row: row, section: 0)) as? PullRequestTableViewCell{
                    cell.cellData = pullRequests[row]
                    return cell
                }
                return nil
            })
            navController?.push(viewController: ViewController.pullRequests(prDataSource))
        }
        
    }
}
