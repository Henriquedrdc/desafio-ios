//
//  RepositoriesDataSource.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 06/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit

final class RepositoriesDataSource:DataSource{
    
    
    let MaxNumOfRepos:Int = 34
    
    typealias Info = Repository
    
    weak var dataManagerDelegate:DataManager?
    weak var activityDelegate:ActivitiyIndicator?
    
    var cellConfig:(Int, UITableView)->(UITableViewCell?)
    
    private var repositoriesRoot:RepositoriesRoot?
    private var isRetrievingData:Bool = false{
        didSet{
            if isRetrievingData{
                activityDelegate?.didStartActivity()
            }
            else{
                activityDelegate?.didFinishActivity()
            }
        }
    }
    private var repositoryNumber:Int = 0
    
    init(cellConfig:@escaping (Int, UITableView)->(UITableViewCell?)){
        self.cellConfig = cellConfig
    }
    
    func dataForItemNumber(_ number:Int) ->Info?{
        if let repositories = repositoriesRoot?.repositories, repositories.count > number{
            return repositories[number]
        }
        return nil
    }
    
    
    public var numberOfItems:Int {
        get{
            if let repositories = repositoriesRoot?.repositories{
                return repositories.count
            }
            return 0
        }
    }
    
    
    public func retrieveNextRepositories(){
        if let repositories = APIResource<RepositoriesRoot>(stringUrl: API.repositories(repositoryNumber+1).endpoint),
            isRetrievingData == false,
            repositoryNumber+1 <= MaxNumOfRepos{
            
            isRetrievingData = true
            WebService().request(repositories, completion: {[weak self] (result) in
                
                self?.isRetrievingData = false
                
                //Update repositoryNumber
                self?.repositoryNumber = (self?.repositoryNumber ?? 0)+1
                
                
                switch result{
                case .error(let error):
                    print(error)
                case .success(let repositoryRoot):
                    if let _ = self?.repositoriesRoot?.repositories, let newRepositories = repositoryRoot.repositories{
                        self?.repositoriesRoot?.repositories?.append(contentsOf: newRepositories)
                    }
                    else{
                        self?.repositoriesRoot = repositoryRoot
                    }
                    self?.dataManagerDelegate?.didReceiveData(data: self?.repositoriesRoot)
                }
            })
        }
    }
    public func retrievePullRequestOfRepository(_ repository:Repository){
        if let authorName = repository.repositoryOwner?.login, let repositoryName = repository.name{
            if let pullRequestResource = APIResource<[PullRequest]>(stringUrl: API.pullRequests(authorName, repositoryName).endpoint){
                activityDelegate?.didStartActivity()
                WebService().request(pullRequestResource, completion: { [weak self](result) in
                    switch result{
                    case .error(let error):
                        print(error)
                    case .success(let pullRequests):
                        self?.dataManagerDelegate?.didReceiveData(data: pullRequests)
                    }
                    self?.activityDelegate?.didFinishActivity()
                })
            }
        }
    }
    
    
}
