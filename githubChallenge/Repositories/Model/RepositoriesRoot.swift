//
//  RepositoriesRoot.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 05/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import Foundation
struct RepositoriesRoot : Codable {
	var repositories : [Repository]?

	enum CodingKeys: String, CodingKey {

		case repositories = "items"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		repositories = try values.decodeIfPresent([Repository].self, forKey: .repositories)
	}

}
