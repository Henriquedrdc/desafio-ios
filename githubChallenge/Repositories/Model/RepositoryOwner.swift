//
//  RepositoryOwner.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 05/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import Foundation
struct RepositoryOwner : Codable {
	let login : String?
	let id : Int?
	let avatarUrl : String?

	enum CodingKeys: String, CodingKey {

		case login
		case id = "id"
		case avatarUrl = "avatar_url"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		login = try values.decodeIfPresent(String.self, forKey: .login)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		avatarUrl = try values.decodeIfPresent(String.self, forKey: .avatarUrl)
	}

}
