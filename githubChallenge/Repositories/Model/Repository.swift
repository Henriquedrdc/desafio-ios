//
//  Repository.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 06/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import Foundation
struct Repository : Codable {
	let id : Int?
	let name : String?
	let repositoryOwner : RepositoryOwner?
	let description : String?
	let forksCount : Int?
	let stargazersCount : Int?

	enum CodingKeys: String, CodingKey {
		case id
		case name = "name"
		case repositoryOwner = "owner"
		case description
		case forksCount = "forks_count"
		case stargazersCount = "stargazers_count"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
        repositoryOwner = try values.decodeIfPresent(RepositoryOwner.self, forKey: .repositoryOwner)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		forksCount = try values.decodeIfPresent(Int.self, forKey: .forksCount)
		stargazersCount = try values.decodeIfPresent(Int.self, forKey: .stargazersCount)
	}

}
