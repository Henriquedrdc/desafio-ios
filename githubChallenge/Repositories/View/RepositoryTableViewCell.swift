//
//  RepositoryTableViewCell.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 07/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit
import Kingfisher

class RepositoryTableViewCell: UITableViewCell {
    @IBOutlet weak var repositoryNameLabel: UILabel!
    @IBOutlet weak var starCountLabel: UILabel!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var starImageView: UIImageView!
    @IBOutlet weak var forkCountLabel: UILabel!
    @IBOutlet weak var forkImageView: UIImageView!
    @IBOutlet weak var repositoryDescriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    var cellData:Repository?{
        didSet{
            if let cellData = cellData{
                repositoryNameLabel.text = cellData.name
                starCountLabel.text = "\(cellData.stargazersCount ?? 0)"
                forkCountLabel.text = "\(cellData.forksCount ?? 0)"
                usernameLabel.text = cellData.repositoryOwner?.login
                
                repositoryDescriptionLabel.text = cellData.description
                if let avatarUrl = URL(string: cellData.repositoryOwner?.avatarUrl ?? ""){
                    avatarImageView.kf.setImage(with: avatarUrl)
                }
            }
            else{
                cleanInfo()
            }
            layoutIfNeeded()
        }
    }
    func cleanInfo(){
        repositoryNameLabel.text = nil
        starCountLabel.text = nil
        usernameLabel.text = nil
        forkCountLabel.text = nil
        repositoryDescriptionLabel.text = nil
        avatarImageView.image = nil
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanInfo()
    }
}
