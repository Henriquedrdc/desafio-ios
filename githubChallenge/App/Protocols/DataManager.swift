//
//  DataManager.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 17/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import Foundation
protocol ActivitiyIndicator:class{
    func didStartActivity()
    func didFinishActivity()
}
protocol DataManager:class{
    func didReceiveData(data: Any?)
}
