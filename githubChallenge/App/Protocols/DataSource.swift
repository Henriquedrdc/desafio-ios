//
//  DataSource.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 17/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit

protocol DataSource:class{
    associatedtype Info
    var numberOfItems:Int {get}
    var cellConfig:(Int, UITableView)->(UITableViewCell?){get set}
    func dataForItemNumber(_ number:Int) ->Info?
}
