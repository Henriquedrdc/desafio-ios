//
//  TableViewAdapter.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 17/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit

protocol TableViewAdapter:class, UITableViewDataSource, UITableViewDelegate{
    var tableView:UITableView {get}
    
    func updateData()
}
