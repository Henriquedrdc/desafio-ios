//
//  WebService.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 05/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import Foundation
import Alamofire

public enum Result<A> {
    case success(A)
    case error(Error)
}

extension Result {
    public init(_ value: A?, or error: Error) {
        if let value = value {
            self = .success(value)
        } else {
            self = .error(error)
        }
    }
    
    public var value: A? {
        guard case .success(let v) = self else { return nil }
        return v
    }
}


public enum WebserviceError: Error {
    case notAuthenticated
    case unableToDecode(Error?)
    case noData
    case other(String?)
}

public final class WebService {
    public init() { }
    
    func request<A>(_ resource: APIResource<A>, completion: @escaping (Result<A>) -> ()){
        
        let completionHandler: (DataResponse<Any>) -> Void = { response in
            let result: Result<A>
            switch response.result{
            case .success:
                if let data = response.data{
                    do{
                        let jsonResponse = try JSONDecoder().decode(A.self, from: data)
                        result = Result.success(jsonResponse)
                    }
                    catch (let error){
                        print(error)
                        result = Result.error(WebserviceError.unableToDecode(error))
                    }
                }
                else{
                    result = Result.error(WebserviceError.noData)
                }
            case .failure(let error):
                result = Result.error(error)
            }
            completion(result)
        }
        switch resource.method {
        case .get:
            Alamofire.request(resource.url, method: .get, headers: nil).responseJSON(completionHandler: completionHandler)
            break
        }
        
    }
}
