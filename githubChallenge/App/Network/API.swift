//
//  API.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 05/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import Foundation

enum API{
    static let host = "https://api.github.com/"
    
    case repositories(Int)
    case pullRequests(String, String)
    
    var endpoint:String{
        switch self {
        case .repositories(let page):
            return "\(API.host)search/repositories?q=language:Java&sort=stars&page=\(page)"
        case .pullRequests(let authorName, let repositoryName):
            return "\(API.host)repos/\(authorName.lowercased())/\(repositoryName.lowercased())/pulls"
        }
    }
    
}
