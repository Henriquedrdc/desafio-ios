//
//  APIResource.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 05/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import Foundation
import Alamofire
public enum ResourceMethod {
    
    case get
    
    public var alamofireMethod: HTTPMethod {
        switch self {
        case .get: return .get
        }
    }
}

public struct APIResource<A:Decodable> {
    public var url: URL
    public var parse: (Data) -> A?
    public var method: ResourceMethod = .get
    
    public init(url: URL, parse: @escaping (Data) -> A?, method: ResourceMethod = .get) {
        self.url = url
        self.parse = parse
        self.method = method
    }
    
}

extension APIResource {
    public init?(stringUrl:String, method: ResourceMethod = .get) {
        if let url = URL(string:stringUrl){
            self.init(url: url, method: method)
        }
        else{
            return nil
        }
    }
    public init(url: URL, method: ResourceMethod = .get) {
        self.url = url
        self.method = method
        self.parse = { data in
            let json = try? JSONDecoder().decode(A.self, from: data)
            return json
        }
    }
}

