//
//  UIViewDesignable.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 18/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit
@IBDesignable
open class UIViewDesignable:UIView{
    
    override open func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = borderWidth
        if shadow{
            
            layer.shadowOpacity = shadowOpacity
            layer.shadowOffset = shadowOffset
            layer.shadowColor = shadowColor?.cgColor
        }
        else{
            layer.shadowOpacity = 0
        }
        if circle{
            if layer.frame.width != layer.frame.height{
                print("UIView width and heigh must be the same when using circular view")
            }
            layer.cornerRadius = frame.width / 2
            layer.masksToBounds = true
        }
        else{
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var shadowOffset: CGSize = CGSize(width: 0, height: 0)
    @IBInspectable var shadowColor: UIColor?
    @IBInspectable var shadow:Bool = false
    @IBInspectable var shadowOpacity: Float = 0
    @IBInspectable var circle: Bool = false
    @IBInspectable var cornerRadius: CGFloat = 0
    @IBInspectable var borderWidth: CGFloat = 0
    @IBInspectable var borderColor: UIColor?
    
    
}
