//
//  UIViewControllerExtension.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 05/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit

extension UIViewController{
    
    var navController:NavController?{
        get{
            if let navController = self.navigationController as? NavController{
                return navController
            }
            else if let navController = UIStoryboard(name: "NavController", bundle: nil).instantiateInitialViewController() as? NavController{
                return navController
            }
            return nil
        }
    }
}
