//
//  PullRequestsViewController.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 06/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit

final class PullRequestsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let CellIdentifier:String = "PullRequestTableViewCell"
    
    var dataSource:PullRequestDataSource?
    var tableViewAdapter:PullRequestViewAdapter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewAdapter = PullRequestViewAdapter(tableView: tableView, dataSource: dataSource, identifier: CellIdentifier, navController:navController)
    }
}
