//
//  PullRequestTableViewCell.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 17/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit
import Kingfisher

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var prDateLabel: UILabel!
    @IBOutlet weak var prDescriptionLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var prTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var cellData:PullRequest?{
        didSet{
            if let cellData = cellData{
                prDescriptionLabel.text = cellData.body
                usernameLabel.text = cellData.prOwner?.login
                if let avatarUrl = URL(string: cellData.prOwner?.avatarUrl ?? ""){
                    avatarImageView.kf.setImage(with: avatarUrl)
                }
                prTitleLabel.text = cellData.title
                prDateLabel.text = "Criado em \(cellData.createdAt ?? "")"
            }
            else{
                cleanInfo()
            }
            layoutIfNeeded()
        }
    }
    func cleanInfo(){
        prDescriptionLabel.text = nil
        usernameLabel.text = nil
        avatarImageView.image = nil
        prTitleLabel.text = nil
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanInfo()
    }
    
}
