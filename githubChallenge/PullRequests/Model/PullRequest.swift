//
//  PullRequest.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 05/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import Foundation
struct PullRequest : Codable {
	let id : Int?
	let prOwner : RepositoryOwner?
	let title : String?
	let body : String?
	let htmlUrl : String?
	let createdAt : String?

	enum CodingKeys: String, CodingKey {

		case id
		case prOwner = "user"
		case title
		case body
		case htmlUrl = "html_url"
		case createdAt = "created_at"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		prOwner = try values.decodeIfPresent(RepositoryOwner.self, forKey: .prOwner)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		body = try values.decodeIfPresent(String.self, forKey: .body)
		htmlUrl = try values.decodeIfPresent(String.self, forKey: .htmlUrl)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let dateString = try values.decodeIfPresent(String.self, forKey: .createdAt), let date = dateFormatter.date(from: dateString){
            dateFormatter.dateFormat = "MMM d, HH:MM"
            createdAt = dateFormatter.string(from: date)
        }
        else{
            createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        }
	}

}
