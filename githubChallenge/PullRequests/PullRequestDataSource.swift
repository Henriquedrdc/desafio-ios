//
//  PullRequestDataSource.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 17/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit


final class PullRequestDataSource:DataSource{
    
    
    typealias Info = PullRequest
    
    var cellConfig: (Int, UITableView) -> (UITableViewCell?)
    
    var pullRequests:[PullRequest]?
    init(pullRequests:[PullRequest]? = nil, cellConfig: @escaping (Int, UITableView) -> (UITableViewCell?)){
        self.cellConfig = cellConfig
        self.pullRequests = pullRequests
    }
    
    
    
    func dataForItemNumber(_ number: Int) -> PullRequest? {
        if let pullRequests = pullRequests{
            return pullRequests[number]
        }
        return nil
    }
    
    var numberOfItems: Int{
        return pullRequests?.count ?? 0
    }
    
}
