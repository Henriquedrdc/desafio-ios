//
//  PullRequestViewAdapter.swift
//  githubChallenge
//
//  Created by Henrique Drumond Rabelo da Costa on 17/02/18.
//  Copyright © 2018 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit


final class PullRequestViewAdapter:NSObject, TableViewAdapter{
    var tableView: UITableView
    private weak var dataSource:PullRequestDataSource?
    var navController:NavController?
    
    init(tableView:UITableView, dataSource:PullRequestDataSource? = nil, identifier:String, navController:NavController? = nil) {
        self.tableView = tableView
        self.navController = navController
        self.dataSource = dataSource
        super.init()
        self.tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
    }
    func updateData() {
        tableView.reloadData()
    }
}
extension PullRequestViewAdapter{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.numberOfItems ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return dataSource?.cellConfig(indexPath.row, tableView) ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let urlString = dataSource?.dataForItemNumber(indexPath.row)?.htmlUrl{
            UIApplication.shared.openURL(URL(string: urlString)!)
        }
    }
}
